// Vector de 6 elementos con 3 tipos de datos diferentes
let vec = [1, 2, 'tres', 'cuatro', true, false];

// a. Imprimir en la consola el vector
console.log('Vector: ',vec);

// b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log('Primer elemento: ',vec[0]); 
console.log('Ultimo elemento: ',vec[vec.length - 1]); 

// c. Modificar el valor del tercer elemento
vec[2] = '3';
console.log('Elemento modificado: ', vec[2]);

// d. Imprimir en la consola la longitud del vector
console.log('Longitud del vector: ',vec.length);

// e. Agregar un elemento al vector usando "push"
vec.push('siete');
console.log('Vector con elemento agregado: ',vec);

// f. Eliminar elemento del final e imprimirlo usando "pop"
let ultimoElemento = vec.pop();
console.log('Elemento eliminado: ',ultimoElemento);
console.log('Vector nuevo: ', vec)

// g. Agregar un elemento en la mitad del vector usando "splice"
vec.splice(vec.length / 2, 0, 'tres y medio');
console.log('Vector modificado: ',vec)

// h. Eliminar el primer elemento usando "shift"
let primerElemento = vec.shift();
console.log('Vector sin el primer elemento: ',vec)

// i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vec.unshift(primerElemento);
console.log('Vector con el primer elemento restaurado: ',vec);
