// Vector de 6 elementos con 3 tipos de datos diferentes
let vec = [1, 2, 'tres', 'cuatro', true, false];

// a. Imprimir en la consola cada valor usando "for"
for (let i = 0; i < vec.length; i++) {
    console.log(vec[i]);
}

// b. Imprimir en la consola cada valor usando "forEach"
vec.forEach(function(elemento) {
    console.log(elemento);
});

// c. Imprimir en la consola cada valor usando "map"
vec.map(function(elemento) {
    console.log(elemento);
});

// d. Imprimir en la consola cada valor usando "while"
let i = 0;
while (i < vec.length) {
    console.log(vec[i]);
    i++;
}

// e. Imprimir en la consola cada valor usando "for..of"
for (let elemento of vec) {
    console.log(elemento);
}